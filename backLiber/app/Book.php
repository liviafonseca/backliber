<?php

namespace App;

use App\Http\Controllers\BookController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Book extends Model
{
    public function create(Request $request){
        $this->name = $request->name;
        $this->category = $request->category;
        $this->price = $request->price;
        $this->autor = $request->autor;
        $this->resume = $request->resume;
        $this->publishingCompany = $request->publishingCompany;
        $this->edition = $request->edition;
        $this->literaryGenre = $request->literaryGenre;
        $this->save();

    }
    
    public function updat(Request $request){
        if($request->name){
            $this->name = $request->name;

        }
        if($request->category){
            $this->category = $request->category;
            
        }
        if($request->price){
            $this->price = $request->price;
            
        }
        if($request->autor){
            $this->autor = $request->autor;
            
        }
        if($request->resume){
            $this->resume = $request->resume;
            
        }
        if($request->publishingCompany){
            $this->publishingCompany = $request->publishingCompany;
            
        }
        if($request->edition){
            $this->edition = $request->edition;
            
        }
        if($request->literaryGenre){
            $this->literaryGenre = $request->literaryGenre;
            
        }
        $this->save();

    }
}
