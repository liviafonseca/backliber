<?php

namespace App;

use App\Http\Controllers\CustomersController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Customers extends Model
{
    public function create(Request $request){
        $this->name = $request->name;
        $this->cpf = $request->cpf;
        $this->email = $request->email;
        $this->telefone = $request->telefone;
        $this->sexo = $request->sexo;
        $this->estado = $request->estado;
        $this->cidade = $request->cidade;
        $this->rua = $request->rua;
        $this->numero = $request->numero;
        $this->dataDeNascimento = $request->dataDeNascimento;
        $this->save();

    }

}
