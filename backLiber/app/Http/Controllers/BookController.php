<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Book;

class BookController extends Controller
{
    public function create(Request $request){
        $book = new Book;
        $book->create($request);
        return response()->json(['book'=> $book],200);

    }

    public function index(){
        $books = Book::all();
        return response()->json(['book' => $books],200);
    }

    public function show($id){
        $book = Book::find($id);
        return response()->json(['book' => $book],200);
    }
    public function updat(Request $request,$id){
        $book = Book::find($id);
        $book->updat($request);
        return response()->json(['book' => $book],200);

    }

    public function destroy($id){
        $book = Book::find($id);
        $book->destroy();
        return response()->json(['Livro deletado com sucesso'],200);


    }


}
