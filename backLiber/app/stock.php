<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class stock extends Model
{
    public function createStock(Request $request){
        $this->quantidadeMaxima = $request->quantidadeMaxima;
        $this->quantidadeCadaLivro = $request->quantidadeCadaLivro;
        $this->cidade = $request->cidade;
        $this->estado = $request->estado;
        $this->numero = $request->numero;
        $this->rua = $request->rua;
        $this->save();

    }
}
