<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BookController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::get('books',[BookController::class,'index']);
// Route::get('books/{id}',[BookController::class,'show']);
// Route::post('books',[BookController::class,'create']);
// Route::put('books/{id}',[BookController::class,'update']);
// Route::delete('books/{id}',[BookController::class,'delete']);

Route::POST('book', 'bookController@create');
Route::GET('book/{id}', 'bookController@show'); 
Route::GET('book', 'bookController@list');
Route::PUT('book/{id}', 'bookController@update');

// Route::POST('costumers', 'costumersController@create');
// Route::GET('costumers/{id}', 'costumersController@show'); 
// Route::GET('costumers', 'costumersController@list');
// Route::PUT('costumers/{id}', 'costumersController@update');